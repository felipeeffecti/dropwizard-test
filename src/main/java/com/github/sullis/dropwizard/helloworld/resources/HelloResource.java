package com.github.sullis.dropwizard.helloworld.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.github.sullis.dropwizard.helloworld.HelloWorldApplication;
import com.github.sullis.dropwizard.helloworld.api.Message;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Path("/hello")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HelloResource {
    private final String defaultName;

    public static AtomicInteger count = new AtomicInteger();

    public HelloResource(final String defaultName) {
        this.defaultName = defaultName;
    }

    @GET
    @Path("/sync")
    public Message get1(@QueryParam("name") Optional<String> name) throws InterruptedException {
        Thread.sleep(5000);
        System.out.println(count.incrementAndGet());
        Message hello = new Message();
        hello.setText(("Hello " + name.orElse(defaultName)));
        return hello;
    }

    @GET
    @Path("/async")
    public Message get2(@QueryParam("name") Optional<String> name) {
        HelloWorldApplication.EXECUTOR.submit(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(count.incrementAndGet());
        });
        Message hello = new Message();
        hello.setText(("Hello " + name.orElse(defaultName)));
        return hello;
    }
}
